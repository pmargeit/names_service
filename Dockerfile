FROM maven:3.8.2-openjdk-11-slim as build

COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -DskipTests

FROM openjdk:11-jre-slim
WORKDIR /app/

COPY --from=build /home/app/target/namesService-0.0.1-SNAPSHOT.jar /app/application.jar
CMD java -jar /app/application.jar