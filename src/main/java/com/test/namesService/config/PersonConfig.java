package com.test.namesService.config;

import com.test.namesService.model.Person;
import com.test.namesService.repository.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class PersonConfig{

    @Bean
    CommandLineRunner commandLineRunner(PersonRepository personRepository){
        return args -> {
            Person patricia= new Person("Patricia Margeit");
            Person eduardo= new Person("Eduardo Moreno");
            personRepository.saveAll(List.of(patricia,eduardo));
        };
    }
}
