package com.test.namesService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NamesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NamesServiceApplication.class, args);
	}


}
