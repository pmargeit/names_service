package com.test.namesService;

import com.test.namesService.config.MessagingConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MessageListener {

    @RabbitListener(queues= MessagingConfig.QUEUE)
    public void listener(Object message){
        System.out.println("Queue: "+ LocalDateTime.now()+" Message: "+message.toString());
    }
}
