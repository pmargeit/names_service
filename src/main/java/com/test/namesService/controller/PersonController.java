package com.test.namesService.controller;

import com.test.namesService.config.MessagingConfig;
import com.test.namesService.model.Person;
import com.test.namesService.service.PersonService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public ResponseEntity<List> getPersons(){
        List<Person> personList=personService.getPersons();
        rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE,MessagingConfig.ROUTINGKEY,personList);
        return new ResponseEntity<List>(personList, HttpStatus.OK);
    }

    @GetMapping (path = "{personId}")
    public ResponseEntity<Person> getPersons(@PathVariable("personId") Long personId){
        Person person=personService.getPerson(personId);
        rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE,MessagingConfig.ROUTINGKEY,person);
        return new ResponseEntity<Person>(person, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Person> registerNewPerson(@RequestBody Person person){
        personService.addNewPerson(person);
        rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE,MessagingConfig.ROUTINGKEY, person);
        return new ResponseEntity<Person>(person, HttpStatus.OK);
    }

    @DeleteMapping (path = "{personId}")
    public ResponseEntity<String> deletePerson(@PathVariable("personId") Long personId){
        String message="Person id "+ personId.toString()+" deleted";
        personService.deletePerson(personId);
        rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE,MessagingConfig.ROUTINGKEY,message);
        return new ResponseEntity<String>(message, HttpStatus.OK);
    }

    @PutMapping (path = "{personId}")
    public ResponseEntity<String> updatePerson(@PathVariable("personId") Long personId,
                             @RequestParam("personName") String personName){
        String message="Person id "+ personId.toString()+" with name "+personName+" updated";
        personService.updatePerson(personId,personName);
        rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE,MessagingConfig.ROUTINGKEY,message);
        return new ResponseEntity<String>(message, HttpStatus.OK);
    }
}

