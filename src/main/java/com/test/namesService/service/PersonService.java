package com.test.namesService.service;

import com.test.namesService.exception.MethodArgumentNotValidException;
import com.test.namesService.exception.RecordNotFoundException;
import com.test.namesService.model.Person;
import com.test.namesService.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getPersons(){
        return personRepository.findAll();
    }

    public Person getPerson(Long personId) {
        Person person=personRepository.findById(personId)
                .orElseThrow(()-> new RecordNotFoundException("Person with id "+personId+" does not exists"));
        return person;
    }


    public void addNewPerson(Person person) {
        Optional<Person> personOptional=personRepository.findPersonByName(person.getName());
        if(personOptional.isPresent()){
            throw new MethodArgumentNotValidException("Name "+person.getName()+" taken");
        }
        personRepository.save(person);
        System.out.println(person);
    }

    public void deletePerson(Long personId) {
        boolean exists=personRepository.existsById(personId);
        if(!exists){
            throw new RecordNotFoundException("Person with id "+personId+" does not exists");
        }
        personRepository.deleteById(personId);
    }

    @Transactional
    public void updatePerson(Long personId, String personName) {
        Person person=personRepository.findById(personId)
                .orElseThrow(()-> new RecordNotFoundException("Person with id "+personId+" does not exists"));
        if (personName!=null && personName.trim().length()>0 && !person.getName().equals(personName)){
            Optional<Person> personOptional=personRepository.findPersonByName(personName);
            if(personOptional.isPresent()){
                throw new MethodArgumentNotValidException("Name "+person.getName()+" taken");
            }
            person.setName(personName);
        }
        else{
                throw new MethodArgumentNotValidException("Name cannot be void or empty or same");
        }
    }
}
